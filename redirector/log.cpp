#include "log.h"

void Log(const wchar_t *fmt, ...)
{
	va_list list;
	static wchar_t buffer[1024];
	static DWORD w;
	FILE* f;

	va_start(list, fmt);

	// to console
	DWORD len = wvsprintf(buffer, fmt, list);
	WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), buffer, len, (DWORD *)&w, NULL);

	if (fopen_s(&f, "redirector.log", "a+") == 0)
	{
		vfwprintf(f, fmt, list);
		fflush(f);
		fclose(f);
	}

	va_end(list);
}

void LogA(const char *fmt, ...)
{
	va_list list;
	static char buffer[1024];
	static DWORD w;

	va_start(list, fmt);

	// to console
	DWORD len = sprintf(buffer, fmt, list);
	WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE), buffer, len, (DWORD *)&w, NULL);

/*	if (fopen_s(&f, "bypass.log", "a+") == 0)
	{
		vfwprintf(f, fmt, list);
		fflush(f);
		fclose(f);
	}*/

	va_end(list);
}