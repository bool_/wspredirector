#include "util.h"

// code from moose
unsigned int FindAoB(unsigned int start, unsigned int end, unsigned char *pattern, unsigned int length, unsigned char wildcard)
{
	bool found = false;

	for (unsigned int i = start; i < end - length; i++)
	{
		for (unsigned int j = 0; j < length; j++)
		{
			if (pattern[j] == wildcard) continue;
			if (*(unsigned char *)(i + j) != pattern[j])
			{
				found = false;
				break;
			}

			found = true;
		}

		if (found) return i;
	}

	return 0;
}