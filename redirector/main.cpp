#pragma comment(lib, "detours/detours.lib")
#pragma comment(lib, "ws2_32.lib")

#include <WinSock2.h>
#include <Windows.h>
#include "detours/detours.h"

#include "log.h"
#include "util.h"

typedef int (__stdcall* pWSPConnect) (SOCKET s, const struct sockaddr *name, int namelen, LPWSABUF lpCallerData, LPWSABUF lpCalleeData, LPQOS lpSQOS, LPQOS lpGQOS, LPINT lpErrno);
typedef int (__stdcall* pWSPGetPeerName) (SOCKET s, struct sockaddr *name, LPINT namelen, LPINT lpErrno); 

pWSPConnect _WSPConnect = NULL;
pWSPGetPeerName _WSPGetPeerName = NULL;
pWSPConnect WSPConnect_ = NULL;
pWSPGetPeerName WSPGetPeerName_ = NULL;

PDWORD PCONTEXT_TABLE = (PDWORD) 0x00000000;
PDWORD (__stdcall * WahReferenceContextByHandle)( PDWORD context_table, SOCKET handle);

DWORD dwLoginServer;

// code thanks to some weird german guy
void AcquireWSPFunctions()
{ 
	WSADATA wsadata;
	if((WSAStartup(MAKEWORD(2,2), &wsadata))!=NO_ERROR)
	{
		Log(L"Error with WSAStartup: %d\n", GetLastError());
		return;
	}
	*(FARPROC *)&WahReferenceContextByHandle=GetProcAddress(LoadLibrary(L"ws2help.dll"), "WahReferenceContextByHandle");
	if(!WahReferenceContextByHandle)
		return;
	SOCKET sock = socket(AF_INET,SOCK_STREAM,0);
	PDWORD PWSHANDLE_CONTEXT = WahReferenceContextByHandle((PDWORD)(*PCONTEXT_TABLE), sock);
	if(!PWSHANDLE_CONTEXT)
		return; 
	// connect		: call dword ptr [esi+24]
	// getpeername	: call dword ptr [eax+38]
	// recv			: call dword ptr [eax+54]
	// recvfrom		: call dword ptr [eax+5C]
	// send			: call dword ptr [eax+64]
	// sento		: call dword ptr [eax+6C]
	PDWORD PWSPPROC_TABLE = (PDWORD)*(PDWORD)((PBYTE)PWSHANDLE_CONTEXT+0xC);
	WSPGetPeerName_ = (pWSPGetPeerName) *(PDWORD)((PBYTE)PWSPPROC_TABLE+0x38);
	Log(L"WSPGetPeerName address: %X\n", (DWORD) WSPGetPeerName_);
	WSPConnect_ = (pWSPConnect) *(PDWORD)((PBYTE)PWSPPROC_TABLE+0x24);
	Log(L"WSPConnect address: %X\n", (DWORD) WSPConnect_);
	if(sock)
		closesocket(sock);
	return;
} 

int __stdcall WSPGetPeerName_detour(SOCKET s, struct sockaddr *name, LPINT namelen, LPINT lpErrno) {
	int ret = _WSPGetPeerName(s, name, namelen, lpErrno);
	wchar_t buf[50];
	DWORD len = 50;
	WSAAddressToString((sockaddr*) name, *namelen, NULL, buf, &len);
	Log(L"WSPGetPeerName called with socket addr %s\n", buf);
	if (wcsstr(_wcslwr(buf), L"209.105.247.42") != 0) {
		Log(L"Found IP check, patching\n", buf, "209.105.247.42:8484");
		sockaddr_in* service = (sockaddr_in*) name;
		unsigned long address = dwLoginServer;
		memcpy(&service->sin_addr, &address, sizeof(unsigned long));
	}
	return ret;
}

int __stdcall WSPConnect_detour(SOCKET s, const struct sockaddr *name, int namelen, LPWSABUF lpCallerData, LPWSABUF lpCalleeData, LPQOS lpSQOS, LPQOS lpGQOS, LPINT lpErrno) {
	wchar_t buf[50];
	DWORD len = 50;
	WSAAddressToString((sockaddr*) name, namelen, NULL, buf, &len);
	Log(L"WSPConnect called with %s\n", buf);
	if (wcsstr(_wcslwr(buf), L":8484") != 0) {
		sockaddr_in* service = (sockaddr_in*) name;

		dwLoginServer = (DWORD)&service->sin_addr;

		unsigned long address = inet_addr("209.105.247.42"); // push it to my server.
		memcpy(&service->sin_addr, &address, sizeof(unsigned long));
	}
	return _WSPConnect(s, name, namelen, lpCallerData, lpCalleeData, lpSQOS, lpGQOS, lpErrno);
}

DWORD WINAPI DllThreadProc() {
	HANDLE dwWS = LoadLibrary(L"WS2_32.dll");
	Log(L"WS2_32.dll found at: %X\n", dwWS);
	IMAGE_NT_HEADERS *nth = (IMAGE_NT_HEADERS *)((unsigned int)dwWS + PIMAGE_DOS_HEADER(dwWS)->e_lfanew);
	DWORD imgStart = (unsigned int)dwWS;
    DWORD imgEnd = imgStart + nth->OptionalHeader.SizeOfImage;
	unsigned char scan_pattern[] = {0x8B, 0xFF, 0x55, 0x8B, 0xEC, 0xFF, 0x75, 0x08, 0xFF, 0x35};
	unsigned int position = FindAoB(imgStart, imgEnd, scan_pattern, _countof(scan_pattern));
	position += 10;
	PCONTEXT_TABLE = (*(PDWORD*)position);
	Log(L"PSM_CONTEXT_TABLE found at: %X\n", PCONTEXT_TABLE);
	AcquireWSPFunctions();
	_WSPConnect = (pWSPConnect) DetourFunction((PBYTE) WSPConnect_, (PBYTE) WSPConnect_detour);
	_WSPGetPeerName = (pWSPGetPeerName) DetourFunction((PBYTE) WSPGetPeerName_, (PBYTE) WSPGetPeerName_detour);
	return 0;
}


BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
	DisableThreadLibraryCalls(hinstDLL);
	switch(fdwReason) {
	case DLL_PROCESS_ATTACH:
		AllocConsole();
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE) DllThreadProc, NULL, NULL, NULL);
		break;
	case DLL_PROCESS_DETACH:
		FreeConsole();
		break;
	}
	return true;
}